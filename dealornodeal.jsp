<%--Jesse Byers--%>
<%--c3162857--%>
<%--Assignment 2--%>
<%--dealornodeal.jsp--%>
<%--14/09/17--%>


<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ page import="caseinfo.Case" %>
<%@ page import="caseinfo.Current" %>

<jsp:useBean id="case1" class="caseinfo.Case" scope="session" />
<jsp:useBean id="case2" class="caseinfo.Case" scope="session" />
<jsp:useBean id="case3" class="caseinfo.Case" scope="session" />
<jsp:useBean id="case4" class="caseinfo.Case" scope="session" />
<jsp:useBean id="case5" class="caseinfo.Case" scope="session" />
<jsp:useBean id="case6" class="caseinfo.Case" scope="session" />
<jsp:useBean id="case7" class="caseinfo.Case" scope="session" />
<jsp:useBean id="case8" class="caseinfo.Case" scope="session" />
<jsp:useBean id="case9" class="caseinfo.Case" scope="session" />
<jsp:useBean id="case10" class="caseinfo.Case" scope="session" />
<jsp:useBean id="case11" class="caseinfo.Case" scope="session" />
<jsp:useBean id="case12" class="caseinfo.Case" scope="session" />

<jsp:useBean id="currentgame" class="caseinfo.Current" scope="session" />

<!DOCTYPE html>


<meta charset="utf-8"/>
<link rel="stylesheet" type="text/css" href="/c3162857_assignment2/stylesheet.css"/>



<html>


<head>
  <title>Deal or No Deal</title>
</head>

<body>


<h1>Deal or No Deal</h1>


		

<div>
<p>Please click one of the briefcases below.</p>

<p>current turn: <%=currentgame.getTurn()%></p>


<p>current round: <%=currentgame.getRound()%></p>
</div>
<br>
<br>


<%-- Below is the table for all of the cases --%>
	<table>

		<% int casenum = 1;%>
	
		
		<%boolean casebool;%>
		<%float casevalue1;%>
		
		
		
		<% int rownum = 1, colnum = 1; %>
		
		<% while (rownum <= 4) { %>
		<tr>
			<% while (colnum <= 3) { %>
			
			<%-- The below if statements sets the values so the casenum can be matched up with the pictures in the loop--%>
					<%if (casenum == 1){		
			boolean caseopen = case1.getOpen();
			casebool = caseopen;
			float casevalue = case1.getValue();
			casevalue1 = casevalue;
			
		} else if (casenum == 2){
			boolean caseopen = case2.getOpen();
			casebool = caseopen;
			float casevalue = case2.getValue();
			casevalue1 = casevalue;
			
		}else if (casenum == 3){
			boolean caseopen = case3.getOpen();
			casebool = caseopen;
			float casevalue = case3.getValue();
			casevalue1 = casevalue;
			
		}else if (casenum == 4){
			boolean caseopen = case4.getOpen();
			casebool = caseopen;
			float casevalue = case4.getValue();
			casevalue1 = casevalue;
			
		}else if (casenum == 5){
			boolean caseopen = case5.getOpen();
			casebool = caseopen;
			float casevalue = case5.getValue();
			casevalue1 = casevalue;
			
		}else if (casenum == 6){
			boolean caseopen = case6.getOpen();
			casebool = caseopen;
			float casevalue = case6.getValue();
			casevalue1 = casevalue;
			
		}else if (casenum == 7){
			boolean caseopen = case7.getOpen();
			casebool = caseopen;
			float casevalue = case7.getValue();
			casevalue1 = casevalue;
			
		}else if (casenum == 8){
			boolean caseopen = case8.getOpen();
			casebool = caseopen;
			float casevalue = case8.getValue();
			casevalue1 = casevalue;
			
		}else if (casenum == 9){
			boolean caseopen = case9.getOpen();
			casebool = caseopen;
			float casevalue = case9.getValue();
			casevalue1 = casevalue;
			
		}else if (casenum == 10){
			boolean caseopen = case10.getOpen();
			float casevalue = case10.getValue();
			casevalue1 = casevalue;
			casebool = caseopen;
			
		}else if (casenum == 11){
			boolean caseopen = case11.getOpen();
			casebool = caseopen;
			float casevalue = case11.getValue();
			casevalue1 = casevalue;
			
		}else{
			boolean caseopen = case12.getOpen();
			casebool = caseopen;
			float casevalue = case12.getValue();
			casevalue1 = casevalue;
			
		}%>

		<%-- Opened cases are crossed out --%>
			<% if ( casebool == true) { %>
			
			<td><img src="images/opencross/briefcaseopen<%=casenum%>c.jpg" alt="briefcase <%=casenum%> image not found" style=></td>
			
			<% } else { %>
		
			<%--Closed cases--%>
			<td><a href="/c3162857_assignment2/briefcaseopen.jsp?case=<%=casenum%>&value=<%=casevalue1%>" onClick=""><img src="images/closed/briefcase<%=casenum%>.jpg" alt="briefcase <%=casenum%> image not found" style=></a></td>
			
			<% } %>
			
			<%casenum++;%>
			<% colnum++; %>
			<%}%>
		</tr>
		<% rownum++; colnum = 1; %>
		<% } %>
		
	</table>
				

<nav>
		<ul>
		<li><a href="http://localhost:8080/c3162857_assignment2/Index.jsp">Exit</a></li>
		</ul>
		</nav>
		
<hr>

<address>&copy;2017 Copyright Mr. Jesse Byers jesse.byers@uon.edu.au</address>

</body>

</html>

