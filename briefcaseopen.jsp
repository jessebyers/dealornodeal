<%--Jesse Byers--%>
<%--c3162857--%>
<%--Assignment 2--%>
<%--briefcaseopen.jsp--%>
<%--14/09/17--%>

<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ page import="caseinfo.Case" %>
<%@ page import="caseinfo.Current" %>

<!--Gets the case beans and current game position-->
<jsp:useBean id="case1" class="caseinfo.Case" scope="session" />
<jsp:useBean id="case2" class="caseinfo.Case" scope="session" />
<jsp:useBean id="case3" class="caseinfo.Case" scope="session" />
<jsp:useBean id="case4" class="caseinfo.Case" scope="session" />
<jsp:useBean id="case5" class="caseinfo.Case" scope="session" />
<jsp:useBean id="case6" class="caseinfo.Case" scope="session" />
<jsp:useBean id="case7" class="caseinfo.Case" scope="session" />
<jsp:useBean id="case8" class="caseinfo.Case" scope="session" />
<jsp:useBean id="case9" class="caseinfo.Case" scope="session" />
<jsp:useBean id="case10" class="caseinfo.Case" scope="session" />
<jsp:useBean id="case11" class="caseinfo.Case" scope="session" />
<jsp:useBean id="case12" class="caseinfo.Case" scope="session" />

<jsp:useBean id="currentgame" class="caseinfo.Current" scope="session" />

<!DOCTYPE html>


<meta charset="utf-8"/>
<link rel="stylesheet" type="text/css" href="/c3162857_assignment2/stylesheet.css"/>

<html>

<script type="text/javascript">

 function dealButton () {
	 
	 alert("You WON!");
	 window.location = "http://localhost:8080/c3162857_assignment2/Index.jsp";
	 
	 
 }
 
 function noDealButton(){
	 
	 window.location =  "http://localhost:8080/c3162857_assignment2/dealornodeal.jsp";
	 
 }
 

function nextTurn () {
	
	 window.location = "http://localhost:8080/c3162857_assignment2/dealornodeal.jsp";
	
}

function returnHome(){
	
	window.location = "http://localhost:8080/c3162857_assignment2/Index.jsp";
	
}


</script>


<head>
  <title>Deal or No Deal</title>
</head>

<body>


<h1>Deal or No Deal</h1>


<div>


<%int turnno = currentgame.getTurn(); %>

<%currentgame.setRound(turnno);%>

<%turnno++;%>
<%currentgame.setTurn(turnno);%>

<%String caseid = request.getParameter("case");%>
<%String valueid = request.getParameter("value");%>


<%int caseidint = Integer.parseInt(caseid);%>


<p>YOU SELECTED CASE: <%=caseidint%> </p>


<!--below is used to set the cases state when they are clicked, so the proper picture needed on our select page is used-->
<%if (caseidint == 1) {
	case1.setOpen(true);
	
} else if (caseidint == 2) {
	case2.setOpen(true);
	
}else if (caseidint == 3) {
	case3.setOpen(true);
	
}else if (caseidint == 4) {
	case4.setOpen(true);
	
}else if (caseidint == 5) {
	case5.setOpen(true);
	
}else if (caseidint == 6) {
	case6.setOpen(true);
	
}else if (caseidint == 7) {
	case7.setOpen(true);
	
}else if (caseidint == 8) {
	case8.setOpen(true);
	
}else if (caseidint == 9) {
	case9.setOpen(true);
	
}else if (caseidint == 10) {
	case10.setOpen(true);
	
}else if (caseidint == 11) {
	case11.setOpen(true);
	
}else {
	case12.setOpen(true);
	
}


%>


<br>
<p1>VALUE:$<%=valueid%> </p1>
</div>
<br>

<img src="images/open/briefcaseopen<%=caseid%>.jpg" alt="briefcase <%=caseid%> image not found" style=></td>

<!--Offer code  formula: the total amount of the money in the remaining cases/ by the number of cases-->
<%
double offervalue = 0;
double opencount = 0;

if (case1.getOpen() == false){
	offervalue += case1.getValue();
	opencount++;
} 
if (case2.getOpen() == false){
	offervalue += case2.getValue();
	opencount++;
}
if (case3.getOpen() == false){
	offervalue += case3.getValue();
	opencount++;
}
if (case4.getOpen() == false){
	offervalue += case4.getValue();
	opencount++;
}
if (case5.getOpen() == false){
	offervalue += case5.getValue();
	opencount++;
}
if (case6.getOpen() == false){
	offervalue += case6.getValue();
	opencount++;
}
if (case7.getOpen() == false){
	offervalue += case7.getValue();
	opencount++;
}
if (case8.getOpen() == false){
	offervalue += case8.getValue();
	opencount++;
}
if (case9.getOpen() == false){
	offervalue += case9.getValue();
	opencount++;
}
if (case10.getOpen() == false){
	offervalue += case10.getValue();
	opencount++;
}
if (case11.getOpen() == false){
	offervalue += case11.getValue();
	opencount++;
}
if (case12.getOpen() == false){
	offervalue += case12.getValue();
	opencount++;
}

offervalue = offervalue/opencount;

%>




<!--Below is the deal or no deal output that appear after every round-->
<% 
if (currentgame.getTurn()== 5){
	
	out.print("<div><p>Offer is : $" + offervalue + "       Deal or No Deal? </p></div>");
	out.print("<div class=\"buttons\">");
	out.print("<input type=\"submit\" value=\"Deal\" onclick=\"dealButton()\"/> ");
	out.print("<input type=\"submit\" value=\"No Deal\" onclick=\"noDealButton()\"/>");
	out.print("</div>");


} else if (currentgame.getTurn()== 8){
	
	out.print("<div><p>Offer is : $" + offervalue + "      Deal or No Deal? </p></div>");
	out.print("<div class=\"buttons\">");
	out.print("<input type=\"submit\" value=\"Deal\" onclick=\"dealButton()\"/> ");
	out.print("<input type=\"submit\" value=\"No Deal\" onclick=\"noDealButton()\"/>");
	out.print("</div>");


} else if (currentgame.getTurn()== 10){
	
	out.print("<div><p>Offer is : $" + offervalue + "      Deal or No Deal? </p></div>");
	out.print("<div class=\"buttons\">");
	out.print("<input type=\"submit\" value=\"Deal\" onclick=\"dealButton()\"/> ");
	out.print("<input type=\"submit\" value=\"No Deal\" onclick=\"noDealButton()\"/>");
	out.print("</div>");


} else if (currentgame.getTurn()== 11){
	
	out.print("<div><p>Offer is : $" + offervalue + "      Deal or No Deal? </p></div>");
	out.print("<div class=\"buttons\">");
	out.print("<input type=\"submit\" value=\"Deal\" onclick=\"dealButton()\"/> ");
	out.print("<input type=\"submit\" value=\"No Deal\" onclick=\"noDealButton()\"/>");
	out.print("</div>");


} else if (currentgame.getTurn()== 12){
	
	
	out.print("<div><p>The last offer is : $" + offervalue + "      Deal or No Deal? </p></div>");
	out.print("<div class=\"buttons\">");
	out.print("<input type=\"submit\" value=\"Deal\" onclick=\"dealButton()\"/> ");
	out.print("<input type=\"submit\" value=\"No Deal\" onclick=\"noDealButton()\"/>");
	out.print("</div>");

} else if (currentgame.getTurn()== 13){
	
	
	out.print("<div><p>YOU HAVE WON: $" + valueid + "</p></div>");
	out.print("<div class=\"buttons\">");
	out.print("<input type=\"submit\" value=\"Continue\" onclick=\"returnHome()\"/> ");
	out.print("</div>");

}


%>

 

<br>

<!--Last round, so the user cannot go back to the selection page-->
<%if (currentgame.getTurn()!= 13){%>
<div class="buttons">
<input type="submit" value="Continue" onclick="nextTurn()"/> 
</div>
<br>
<%}%>
<%offervalue=0;
opencount = 12;
%>

<nav>
		<ul>
		<li><a href="http://localhost:8080/c3162857_assignment2/Index.jsp">Exit</a></li>
		</ul>
		</nav>
		
<hr>

<address>&copy;2017 Copyright Mr. Jesse Byers jesse.byers@uon.edu.au</address>

</body>

</html>