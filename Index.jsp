<%--Jesse Byers--%>
<%--c3162857--%>
<%--Assignment 2--%>
<%--Index.jsp--%>
<%--14/09/17--%>


<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ page import="caseinfo.Case" %>
<%@ page import="caseinfo.Current" %>

<!--Case1-->
<jsp:useBean id="case1" class="caseinfo.Case" scope="session" />
<jsp:setProperty name="case1" property="name" value="Case 1" />
<jsp:setProperty name="case1" property="open" value="false" />
<jsp:setProperty name="case1" property="value" value="0.5" />
<!--Case2-->
<jsp:useBean id="case2" class="caseinfo.Case" scope="session" />
<jsp:setProperty name="case2" property="name" value="Case 2" />
<jsp:setProperty name="case2" property="open" value="false" />
<jsp:setProperty name="case2" property="value" value="1" />
<!--Case3-->
<jsp:useBean id="case3" class="caseinfo.Case" scope="session" />
<jsp:setProperty name="case3" property="name" value="Case 3" />
<jsp:setProperty name="case3" property="open" value="false" />
<jsp:setProperty name="case3" property="value" value="10" />
<!--Case4-->
<jsp:useBean id="case4" class="caseinfo.Case" scope="session" />
<jsp:setProperty name="case4" property="name" value="Case 4" />
<jsp:setProperty name="case4" property="open" value="false" />
<jsp:setProperty name="case4" property="value" value="100" />
<!--Case5-->
<jsp:useBean id="case5" class="caseinfo.Case" scope="session" />
<jsp:setProperty name="case5" property="name" value="Case 5" />
<jsp:setProperty name="case5" property="open" value="false" />
<jsp:setProperty name="case5" property="value" value="200" />
<!--Case6-->
<jsp:useBean id="case6" class="caseinfo.Case" scope="session" />
<jsp:setProperty name="case6" property="name" value="Case 6" />
<jsp:setProperty name="case6" property="open" value="false" />
<jsp:setProperty name="case6" property="value" value="500" />
<!--Case7-->
<jsp:useBean id="case7" class="caseinfo.Case" scope="session" />
<jsp:setProperty name="case7" property="name" value="Case 7" />
<jsp:setProperty name="case7" property="open" value="false" />
<jsp:setProperty name="case7" property="value" value="1000" />
<!--Case8-->
<jsp:useBean id="case8" class="caseinfo.Case" scope="session" />
<jsp:setProperty name="case8" property="name" value="Case 8" />
<jsp:setProperty name="case8" property="open" value="false" />
<jsp:setProperty name="case8" property="value" value="2000" />
<!--Case9-->
<jsp:useBean id="case9" class="caseinfo.Case" scope="session" />
<jsp:setProperty name="case9" property="name" value="Case 9" />
<jsp:setProperty name="case9" property="open" value="false" />
<jsp:setProperty name="case9" property="value" value="5000" />
<!--Case10-->
<jsp:useBean id="case10" class="caseinfo.Case" scope="session" />
<jsp:setProperty name="case10" property="name" value="Case 10" />
<jsp:setProperty name="case10" property="open" value="false" />
<jsp:setProperty name="case10" property="value" value="10000" />
<!--Case11-->
<jsp:useBean id="case11" class="caseinfo.Case" scope="session" />
<jsp:setProperty name="case11" property="name" value="Case 11" />
<jsp:setProperty name="case11" property="open" value="false" />
<jsp:setProperty name="case11" property="value" value="20000" />
<!--Case12-->
<jsp:useBean id="case12" class="caseinfo.Case" scope="session" />
<jsp:setProperty name="case12" property="name" value="Case 12" />
<jsp:setProperty name="case12" property="open" value="false" />
<jsp:setProperty name="case12" property="value" value="50000" />

<!--Sets game logic for coutning turns and rounds-->
<jsp:useBean id="currentgame" class="caseinfo.Current" scope="session" />
<jsp:setProperty name="currentgame" property="turn" value="1" />
<jsp:setProperty name="currentgame" property="round" value="1" />


<!DOCTYPE html>


<meta charset="utf-8"/>
<link rel="stylesheet" type="text/css" href="/c3162857_assignment2/stylesheet.css"/>



<html>
<script type="text/javascript">

<!--link used to start a new game-->
function newGame () {
	
	 window.location = "http://localhost:8080/c3162857_assignment2/dealornodeal.jsp";
	
}

</script>


<head>
  <title>Deal or No Deal</title>
</head>

<body>


<h1>Deal or No Deal</h1>


<div>
<p>Welome to Deal or No Deal</p>
<p>Please click on the button below to start a new game</p>

</div>
<br>
<img src="images/briefcasemoney.jpg" alt="image not found" style=>
<br>
<div class="buttons">
<input type="submit" value="New Game" onclick="newGame()"/> 
</div>


		
<hr>

<address>&copy;2017 Copyright Mr. Jesse Byers jesse.byers@uon.edu.au</address>

</body>

</html>

