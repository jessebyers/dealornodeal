/*Jesse Byers
c3162857
Assignment 2
Case.java
14/09/17*/

/*BELOW IS THE CODE FOR THE GAMES CASE INFORMATION*/

package caseinfo;


import java.io.Serializable;


public class Case implements java.io.Serializable
{
	private String name;
	private float value;
	private boolean open;

    /*Constructor*/
    public Case(){}

	/*Set and Get for name*/
	 public void setName(String name)
    {
        this.name=name;
		
    }

    public String getName ()
    {
		return this.name;
		
    } 
	

    /*Set and Get for Open*/
    public void setOpen (boolean open)
    {
        this.open = open;
		
    }

    public boolean getOpen ()
    {
		return this.open;
		
    } 
	
	
	/*Set and Get for Value*/
	 public void setValue (float value)
    {
        this.value = value;
		
    }

    public float getValue ()
    {
		return this.value;
		
    } 
	
}
	