/*Jesse Byers
c3162857
Assignment 2
Current.java
14/09/17*/

/*BELOW IS THE CODE FOR THE GAMES LOGIC*/

package caseinfo;


import java.io.Serializable;

public class Current implements java.io.Serializable
{
	private int round;
	private int turn;


    /*Constructor*/
    public Current(){}

	/*Set and Get for round*/
	 public void setRound(int turn)
    {

	 if (turn == 4)
		{
			this.round = 2;
		}

		else if (turn == 7) {
			
			this.round = 3;
		}
		
		else if (turn == 9) {
			
			this.round = 4;
		} 
		
		else if (turn == 10) {
			
			this.round = 5;
			
		} else if (turn == 11) {
			
			this.round = 0;
		}
		
		
    }
	

    public int getRound ()
    {
		return this.round;
		
    } 
		
	
	/*Set and Get for Turn*/
	 public void setTurn (int turn)
    {
        this.turn = turn;
		
    }

    public int getTurn ()
    {
		return this.turn;
		
    }


	
}
	